const express = require('express');
const app = express();

var bodyParser = require('body-parser');

app.use(bodyParser.json());

var path  = require("path");


const { MongoClient } = require("mongodb");
const uri = 'mongodb://localhost:27017';  


const products  = require('./products');


app.disable('etag');

const port = 3006

app.options("/*", function(req, res, next){
    res.header('Access-Control-Allow-Origin', '*');
    res.header('Access-Control-Allow-Methods', 'OPTIONS');
    res.header('Access-Control-Allow-Headers', 'Content-Type, Authorization, Content-Length, X-Requested-With');
    res.send(200);
});

app.get(['/', '/home', '/login', '/products', '/registration', '/about-us', '/product/*'], (request, response) => {   
    response.status(200);
    response.sendFile(path.join(__dirname + '/public/index.html'));
})

app.post('/products', (request, response) => {
    response.setHeader('Access-Control-Allow-Origin', '*');
    response.status(200);
    response.send(products);
})

app.post('/purchase', (request, response) => 
{
    response.setHeader('Access-Control-Allow-Origin', '*');
    response.status(200);

    console.log(request.body);

    (async function()
    {
        try 
        {
            const dbName = 'shop';
            
            const client = await MongoClient.connect(uri,{ useNewUrlParser: true });

            const db = client.db(dbName);

            const collection = db.collection('products');

            collection.insert(request.body);

            client.close();

            response.send({message:'success'});
        
        } catch(e) {
            console.error(e)
        }
    })()
})


app.post('/adduser', (request, response) => 
{
    response.setHeader('Access-Control-Allow-Origin', '*');
    response.status(200);

    console.log(request.body);

    (async function()
    {
        try 
        {
            const dbName = 'shop';
            
            const client = await MongoClient.connect(uri,{ useNewUrlParser: true });

            const db = client.db(dbName);

            const collection = db.collection('user');

            collection.insertOne(request.body);

            client.close();

            response.send({message:'success'});
        
        } catch(e) {
            console.error(e)
        }
    })()
})


var dir = path.join(__dirname, 'public');
app.use(express.static(dir));

app.listen(port, (err) => {
    if (err) {
        return console.log('something bad happened', err)
    }
    console.log(`server is listening on ${port}`)
})