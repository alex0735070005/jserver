const { MongoClient } = require("mongodb");
const uri = 'mongodb://localhost:27017';  

(async function(){
    try {
      const dbName = 'shop';
      const client = await MongoClient.connect(uri,{ useNewUrlParser: true });
      const db = client.db(dbName);
      const collection = db.collection('user');
      console.log(collection.insertOne({name:'Pitya', age:25}));
      client.close();
  
    } catch(e) {
      console.error(e)
    }
  })()